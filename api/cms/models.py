from django.db import models
from django.core import serializers


class Writer(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    profile_pic = models.ImageField()
    biography: models.TextField(max_length=1024)
    active = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['active']


class Article(models.Model):
    id = models.AutoField(primary_key=True)
    written_by = models.ForeignKey(Writer, on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    description = models.TextField(max_length=1024)
    thumbnail = models.ImageField()
    body = models.TextField(max_length=65535)
    published = models.BooleanField(default=False)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['date_created']


class NewsletterEmail(model.Model):
    id = models.AutoField(primary_key=True)
    customer_name = models.CharField(max_length=64)
    customer_email = models.CharField(max_length=128)
    date_created = models.DateTimeField()
    date_modified = models.DateTimeField(default=None)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['date_created']
