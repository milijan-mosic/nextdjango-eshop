from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import *
from .models import *
import bcrypt
import uuid
import random
import string


# INDEX
@api_view(["GET"])
def index(request):
    return HttpResponse("Hello, world!")


# PRODUCTS
@api_view(["GET"])
def get_paginated_products(request, page):
    low = 0
    high = 12
    if page >= 2:
        low = page * 12 - 1
        high = page * 12 + 12
    products = Product.objects.all()[low:high]
    serializer = ProductSerializer(products, many=True)
    return JsonResponse(serializer.data, safe=False)


@api_view(["GET"])
def get_single_product(request, id):
    product = Product.objects.get(id=id)
    serializer = ProductSerializer(product, many=False)
    return JsonResponse(serializer.data, safe=False)


# CUSTOMERS
@api_view(["POST"])
def register_user(request):
    arrived_data = request.data

    if Customer.objects.filter(email=arrived_data["email"]).count() >= 1:
        print("EMAIL IS ALREADY TAKEN.")
        return JsonResponse({"error": "Email is already taken."})

    password_to_hash = bytes(arrived_data["password"].encode("UTF-8"))
    hashed_password = bcrypt.hashpw(password_to_hash, bcrypt.gensalt())
    arrived_data["password"] = hashed_password.decode("UTF-8")
    arrived_data.update({"uuid": uuid.uuid4()})

    new_customer = CustomerSerializer(data=arrived_data)

    if new_customer.is_valid():
        new_customer.save()

    print("REGISTRATION SUCCESSFULL")
    return JsonResponse(new_customer.data["uuid"], safe=False)


@api_view(["POST"])
def login_user(request):
    arrived_data = request.data
    customer_object = Customer.objects.get(email=arrived_data["email"])
    customer = CustomerSerializer(customer_object, many=False)

    if bcrypt.checkpw(arrived_data["password"].encode("UTF-8"), customer.data["password"].encode("UTF-8")):
        print("LOGIN SUCCESSFULL")
        print(customer.data["uuid"])
        return JsonResponse(customer.data["uuid"], safe=False)
    else:
        print("EMAIL AND/OR PASSWORD IS WRONG!")
        return JsonResponse({"error": "Email and/or password is wrong."})


# ORDERS
@api_view(["POST"])
def place_order(request):
    arrived_data = request.data
    order_code = ''.join(random.choice(string.ascii_uppercase +
                         string.ascii_lowercase + string.digits) for _ in range(16))

    customers_queryset = Customer.objects.get(uuid=arrived_data["user_uuid"])
    customers_serializer = CustomerSerializer(customers_queryset, many=False)
    customers_id = customers_serializer.data["id"]

    products_queryset = Product.objects.get(id=arrived_data["wanted_product"])
    products_serializer = ProductSerializer(products_queryset, many=False)
    products_price = products_serializer.data["price"]
    total_price = arrived_data["wanted_quantity"] * products_price

    status = 1

    del arrived_data['user_uuid']
    arrived_data.update({"code": order_code})
    arrived_data.update({"ordered_by": customers_id})
    arrived_data.update({"total_price": total_price})
    arrived_data.update({"status": status})

    new_order = OrderSerializer(data=arrived_data)

    if new_order.is_valid():
        new_order.save()

    print(arrived_data)
    return JsonResponse(new_order.data["code"], safe=False)


@api_view(["POST"])
def get_receipt(request):
    arrived_data = request.data

    if Order.objects.filter(code=arrived_data["code"]).count() == 0:
        print("ORDER DOES NOT EXIST.")
        return JsonResponse({"error": "Order does not exist"})

    order_queryset = Order.objects.get(code=arrived_data["code"])
    order = OrderSerializer(order_queryset, many=False)

    print(order["ordered_by"].value())

    customer_queryset = Customer.objects.get(id=order["ordered_by"])
    customer = CustomerSerializer(customer_queryset, many=False)

    product_queryset = Product.objects.get(id=order["wanted_product"])
    product = ProductSerializer(product_queryset, many=False)

    customer_data = {"full_name": customer["full_name"].value()}
    customer_data.update({"address": customer["address"].value()})
    customer_data.update({"phone": customer["phone"].value()})
    arrived_data.update({"customer_info": customer_data})

    product_info = {"product_name": product["name"].value()}
    product_info.update({"price": product["price"]}.value())
    product_info.update({"wanted_quantity": order["wanted_quantity"]})
    product_info.update({"total_price": order["total_price"]})
    arrived_data.update({"order_info": product_info})

    print(arrived_data)
    return JsonResponse(arrived_data, safe=False)
    # FIX BOUNDFIELD ERROR
