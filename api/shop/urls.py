from django.urls import path
from . import views


urlpatterns = [
    path("", views.index, name="index"),
    path("products/<int:page>", views.get_paginated_products, name="all_products"),
    path("product/<int:id>", views.get_single_product, name="single_products"),
    path("user/register/", views.register_user, name="register"),
    path("user/login/", views.login_user, name="login"),
    path("order/new/", views.place_order, name="place_order"),
    path("order/receipt/", views.get_receipt, name="get_receipt")
]
