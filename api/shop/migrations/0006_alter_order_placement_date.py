# Generated by Django 4.0.5 on 2022-07-03 20:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0005_alter_product_thumbnail'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='placement_date',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
