from django.db import models
import uuid


class Product(models.Model):
    id = models.AutoField(primary_key=True)
    thumbnail = models.ImageField(upload_to='uploads/photos/')
    name = models.CharField(max_length=128)
    description = models.TextField(max_length=1024)
    price = models.FloatField()
    quantity = models.IntegerField()

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


class Customer(models.Model):
    id = models.AutoField(primary_key=True)
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    email = models.EmailField(max_length=128)
    password = models.CharField(max_length=128)
    full_name = models.CharField(max_length=128)
    address = models.TextField(max_length=256)
    phone = models.TextField(max_length=32)

    def __str__(self):
        return self.full_name

    class Meta:
        ordering = ['full_name']


class OrderStatus(models.Model):
    id = models.AutoField(primary_key=True)
    status = models.CharField(max_length=32)

    def __str__(self):
        return self.status

    class Meta:
        ordering = ['status']


class Order(models.Model):
    id = models.AutoField(primary_key=True)
    code = models.CharField(max_length=64)
    ordered_by = models.ForeignKey(Customer, on_delete=models.CASCADE)
    wanted_product = models.ForeignKey(Product, on_delete=models.CASCADE)
    wanted_quantity = models.IntegerField()
    total_price = models.FloatField(default=0)
    placement_date = models.DateTimeField(auto_now_add=True)
    status = models.ForeignKey(OrderStatus, on_delete=models.CASCADE)

    def __str__(self):
        return self.code

    class Meta:
        ordering = ['placement_date']
