import Nav from "../../components/nav";
import Footer from "../../components/footer";
import type { NextPage } from "next";
import Head from "next/head";

const Product: NextPage = () => {
  return (
    <>
      <Head>
        <title>Tea Shop - PDP</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Nav />

      <main>
        <p className="">PDP</p>
      </main>

      <Footer />
    </>
  );
};

export default Product;
