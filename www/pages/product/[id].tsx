import Nav from "../../components/nav";
import Footer from "../../components/footer";
import { GetServerSideProps } from "next";
import type { NextPage } from "next";
import Head from "next/head";
import { useRouter } from "next/router";
import axios from "axios";

export const getServerSideProps: GetServerSideProps = async () => {
  const router = useRouter();
  let { querried_id }: any = router.query;
  const id = querried_id + 1;

  const { product, error } = await axios
    .get("http://localhost:8000/api/v1/shop/product/" + id)
    .then((response) => {
      console.log(response["data"]);
      return response;
    })
    .catch((error) => {
      console.log(error);
      return error;
    });

  return {
    props: {
      product: product,
    },
  };
};

const Product: NextPage = ({ product }: any) => {
  return (
    <>
      <Head>
        <title>Tea Shop - {product.name}</title>
        <meta name="description" content="Made with NextJS" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Nav />

      <main>
        <div className="flex flex-wrap justify-center items-center m-4">
          <div className="w-96 rounded-xl shadow-xl m-4 bg-gray-800">
            <img src={product.thumbnail} className="rounded-xl" />
            <div className="flex flex-col items-center w-full p-4">
              <p className="text-2xl font-bold">{product.name}</p>
              <div className="rounded-xl w-full p-4 mt-2 mb-4 bg-gray-700">
                <p>{product.description}</p>
              </div>
              <div className="flex justify-between items-baseline w-full">
                <p className="text-xl font-semibold">$ {product.price}</p>
                <p>On stock: {product.quantity}</p>
              </div>
              <button type="button" className="p-4 mt-4 rounded-xl shadow-xl transition-all ease-linear duration-100 bg-blue-500 hover:bg-blue-200 hover:text-black">
                Add to cart
              </button>
            </div>
          </div>
        </div>
      </main>

      <Footer />
    </>
  );
};

export default Product;
