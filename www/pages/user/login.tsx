import Nav from "../../components/nav";
import Footer from "../../components/footer";
import type { NextPage } from "next";
import Head from "next/head";
import { useState } from "react";

const Login: NextPage = () => {
  const [email_error, SetEmailError] = useState("");
  const [password_error, SetPasswordError] = useState("");

  const ValidateEmail = () => {
    const email_input = document.getElementById("email_input") as HTMLFormElement;

    if (email_input.value.length === 0) {
      SetEmailError("Don't leave this field empty");
    } else {
      SetEmailError("");
    }
  };

  const ValidatePasswords = () => {
    const password_input = document.getElementById("password_input") as HTMLFormElement;

    if (password_input.value.length === 0) {
      SetPasswordError("Don't leave this field empty");
    } else {
      SetPasswordError("");
    }
  };

  const LoginUser = (event: any) => {
    event.preventDefault();
  };

  return (
    <>
      <Head>
        <title>Tea Shop - Login</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Nav />

      <main className="flex justify-center w-full">
        <div className="flex flex-col justify-center items-center w-80 mt-16 rounded-xl shadow-xl p-6 bg-gray-800">
          <h1 className="text-2xl mb-12">Login</h1>

          <form onSubmit={LoginUser} className="flex flex-col items-center">
            <label>
              Email
              <input
                type="text"
                id="email_input"
                onChange={() => ValidateEmail()}
                className={email_error !== "" ? "w-full p-2 mb-1 rounded-xl shadow-md bg-gray-700 border-2 border-rose-500" : "w-full p-2 mb-1 rounded-xl shadow-md bg-gray-700 border-2 border-gray-700"}
              />
            </label>
            {email_error !== "" ? <p className="mb-4 text-rose-500">{email_error}</p> : <p className="">&nbsp;</p>}

            <label>
              Password
              <input
                type="password"
                id="password_input"
                onChange={() => ValidatePasswords()}
                className={password_error !== "" ? "w-full p-2 mb-1 rounded-xl shadow-md bg-gray-700 border-2 border-rose-500" : "w-full p-2 mb-1 rounded-xl shadow-md bg-gray-700 border-2 border-gray-700"}
              />
            </label>
            {password_error !== "" ? <p className="mb-4 text-rose-500">{password_error}</p> : <p className="">&nbsp;</p>}

            <label className="w-full p-2 mb-1">
              Keep me logged in
              <input type="checkbox" id="kul_checkbox" className="ml-2" />
            </label>

            <input type="submit" value="Submit" className="p-4 mt-4 rounded-xl shadow-xl transition-all ease-linear duration-100 cursor-pointer bg-blue-500 hover:bg-blue-200 hover:text-black" />
          </form>
        </div>
      </main>

      <Footer />
    </>
  );
};

export default Login;
