import Nav from "../../components/nav";
import Footer from "../../components/footer";
import type { NextPage } from "next";
import Head from "next/head";
import { useState } from "react";

const Register: NextPage = () => {
  const [email_error, SetEmailError] = useState("");
  const [password1_error, SetPassword1Error] = useState("");
  const [password2_error, SetPassword2Error] = useState("");
  const [full_name_error, SetFullNameError] = useState("");
  const [address_error, SetAddressError] = useState("");
  const [phone_error, SetPhoneError] = useState("");

  const ValidateEmail = () => {
    const email_input = document.getElementById("email_input") as HTMLFormElement;

    const email_regex: RegExp = /[^@ \t\r\n]+@[^@ \t\r\n]+\.[^@ \t\r\n]+/;
    const email_regex_testing = email_regex.exec(email_input.value);

    if (email_input.value.length === 0) {
      SetEmailError("Don't leave this field empty");
    } else if (email_input.value.length < 9) {
      SetEmailError("Minimum length for an email is 8 characters");
    } else if (email_input.value.length > 65) {
      SetEmailError("Maximum length for an email is 64 characters");
    } else if (!email_regex_testing) {
      SetEmailError("Not a valid email");
    } else {
      SetEmailError("");
    }
  };

  const ValidatePasswords = () => {
    const password1_input = document.getElementById("password1_input") as HTMLFormElement;
    const password2_input = document.getElementById("password2_input") as HTMLFormElement;

    const password_regex: RegExp = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$ %^&*-]).{8,}$/;
    const password_regex_testing = password_regex.exec(password1_input.value);

    if (password1_input.value.length === 0) {
      SetPassword1Error("Don't leave this field empty");
    } else if (password1_input.value.length < 7) {
      SetPassword1Error("Minimum length for a password is 8 characters");
    } else if (password1_input.value.length > 65) {
      SetPassword1Error("Maximum length for a password is 64 characters");
    } else if (!password_regex_testing) {
      SetPassword1Error("Password must contain atleast one uppercase letter, one lowercase letter, one number and one special character");
    } else {
      SetPassword1Error("");
    }

    if (password2_input.value.length === 0) {
      SetPassword2Error("Don't leave this field empty");
    } else if (password1_input.value !== password2_input.value) {
      SetPassword2Error("Passwords don't match");
    } else {
      SetPassword2Error("");
    }
  };

  const ValidateFullName = () => {
    const full_name_input = document.getElementById("full_name_input") as HTMLFormElement;

    const full_name_regex: RegExp = /(^[A-Za-z]{3,16})([ ]{0,1})([A-Za-z]{3,16})?([ ]{0,1})?([A-Za-z]{3,16})?([ ]{0,1})?([A-Za-z]{3,16})/;
    const full_name_regex_testing = full_name_regex.exec(full_name_input.value);

    if (full_name_input.value.length === 0) {
      SetFullNameError("Don't leave this field empty");
    } else if (full_name_input.value.length < 6) {
      SetFullNameError("Minimum length for this field is 6 characters");
    } else if (full_name_input.value.length > 65) {
      SetFullNameError("Maximum length for this field is 64 characters");
    } else if (!full_name_regex_testing) {
      SetFullNameError("Name must contain only alphabetic characters and space (' ') character");
    } else {
      SetFullNameError("");
    }
  };

  const ValidateAddress = () => {
    const address_input = document.getElementById("address_input") as HTMLFormElement;

    const address_regex = /[A-Za-z0-9'\.\-\s\,]/;
    const address_regex_testing = address_regex.exec(address_input.value);

    if (address_input.value.length === 0) {
      SetAddressError("Don't leave this field empty");
    } else if (address_input.value.length < 6) {
      SetAddressError("Minimum length for an address is 6 characters");
    } else if (address_input.value.length > 129) {
      SetAddressError("Maximum length for an address is 128 characters");
    } else if (!address_regex_testing) {
      SetAddressError("Address must not contain special characters");
    } else {
      SetAddressError("");
    }
  };

  const ValidatePhone = () => {
    const phone_input = document.getElementById("phone_input") as HTMLFormElement;

    const phone_regex = /[A-Za-z0-9'\.\-\s\,]/;
    const phone_regex_testing = phone_regex.exec(phone_input.value);

    if (phone_input.value.length === 0) {
      SetPhoneError("Don't leave this field empty");
    } else if (phone_input.value.length < 13) {
      SetPhoneError("Minimum length for a phone number is 12 characters");
    } else if (phone_input.value.length > 26) {
      SetPhoneError("Maximum length for a phone number is 24 characters");
    } else if (!phone_regex_testing) {
      SetPhoneError("Phone can contain numeric, dash (-), plus (+), space (' ') and dot (.) characters only");
    } else {
      SetPhoneError("");
    }
  };

  const RegisterUser = (event: any) => {
    event.preventDefault();
  };

  return (
    <>
      <Head>
        <title>Tea Shop - Register</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Nav />

      <main className="flex justify-center w-full">
        <div className="flex flex-col justify-center items-center w-80 mt-16 rounded-xl shadow-xl p-6 bg-gray-800">
          <h1 className="text-2xl mb-12">Register</h1>

          <form onSubmit={RegisterUser} className="flex flex-col items-center">
            <label>
              Email
              <input
                type="text"
                id="email_input"
                onChange={() => ValidateEmail()}
                className={email_error !== "" ? "w-full p-2 mb-1 rounded-xl shadow-md bg-gray-700 border-2 border-rose-500" : "w-full p-2 mb-1 rounded-xl shadow-md bg-gray-700 border-2 border-gray-700"}
              />
            </label>
            {email_error !== "" ? <p className="mb-4 text-rose-500">{email_error}</p> : <p className="">&nbsp;</p>}

            <label>
              Password
              <input
                type="password"
                id="password1_input"
                onChange={() => ValidatePasswords()}
                className={password1_error !== "" ? "w-full p-2 mb-1 rounded-xl shadow-md bg-gray-700 border-2 border-rose-500" : "w-full p-2 mb-1 rounded-xl shadow-md bg-gray-700 border-2 border-gray-700"}
              />
            </label>
            {password1_error !== "" ? <p className="mb-4 text-rose-500">{password1_error}</p> : <p className="">&nbsp;</p>}

            <label>
              Password again
              <input
                type="password"
                id="password2_input"
                onChange={() => ValidatePasswords()}
                className={password2_error !== "" ? "w-full p-2 mb-1 rounded-xl shadow-md bg-gray-700 border-2 border-rose-500" : "w-full p-2 mb-1 rounded-xl shadow-md bg-gray-700 border-2 border-gray-700"}
              />
            </label>
            {password2_error !== "" ? <p className="mb-4 text-rose-500">{password2_error}</p> : <p className="">&nbsp;</p>}

            <label>
              Full name
              <input
                type="text"
                id="full_name_input"
                onChange={() => ValidateFullName()}
                className={full_name_error !== "" ? "w-full p-2 mb-1 rounded-xl shadow-md bg-gray-700 border-2 border-rose-500" : "w-full p-2 mb-1 rounded-xl shadow-md bg-gray-700 border-2 border-gray-700"}
              />
            </label>
            {full_name_error !== "" ? <p className="mb-4 text-rose-500">{full_name_error}</p> : <p className="">&nbsp;</p>}

            <label>
              Address
              <input
                type="text"
                id="address_input"
                onChange={() => ValidateAddress()}
                className={address_error !== "" ? "w-full p-2 mb-1 rounded-xl shadow-md bg-gray-700 border-2 border-rose-500" : "w-full p-2 mb-1 rounded-xl shadow-md bg-gray-700 border-2 border-gray-700"}
              />
            </label>
            {address_error !== "" ? <p className="mb-4 text-rose-500">{address_error}</p> : <p className="">&nbsp;</p>}

            <label>
              Phone number
              <input
                type="tel"
                id="phone_input"
                onChange={() => ValidatePhone()}
                className={phone_error !== "" ? "w-full p-2 mb-1 rounded-xl shadow-md bg-gray-700 border-2 border-rose-500" : "w-full p-2 mb-1 rounded-xl shadow-md bg-gray-700 border-2 border-gray-700"}
              />
            </label>
            {phone_error !== "" ? <p className="mb-4 text-rose-500">{phone_error}</p> : <p className="">&nbsp;</p>}

            <input type="submit" id="submit_button" value="Submit" className="p-4 rounded-xl shadow-xl transition-all ease-linear duration-100 cursor-pointer bg-blue-500 hover:bg-blue-200 hover:text-black" />
          </form>
        </div>
      </main>

      <Footer />
    </>
  );
};

export default Register;
