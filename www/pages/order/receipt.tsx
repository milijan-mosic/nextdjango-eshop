import Nav from "../../components/nav";
import Footer from "../../components/footer";
import type { NextPage } from "next";
import Head from "next/head";

const Receipt: NextPage = () => {
  return (
    <>
      <Head>
        <title>Tea Shop - Receipt</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Nav />

      <main>
        <p className="">Receipt</p>
      </main>

      <Footer />
    </>
  );
};

export default Receipt;
