import Nav from "../../components/nav";
import Footer from "../../components/footer";
import type { NextPage } from "next";
import Head from "next/head";

const Checkout: NextPage = () => {
  return (
    <>
      <Head>
        <title>Tea Shop - Checkout</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Nav />

      <main>
        <p className="">Checkout</p>
      </main>

      <Footer />
    </>
  );
};

export default Checkout;
