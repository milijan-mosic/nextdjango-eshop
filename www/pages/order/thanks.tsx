import Nav from "../../components/nav";
import Footer from "../../components/footer";
import type { NextPage } from "next";
import Head from "next/head";

const Thanks: NextPage = () => {
  return (
    <>
      <Head>
        <title>Tea Shop - Thanks</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Nav />

      <main>
        <p className="">Thanks</p>
      </main>

      <Footer />
    </>
  );
};

export default Thanks;
