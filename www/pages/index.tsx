import Nav from "../components/nav";
import Footer from "../components/footer";
import { GetServerSideProps } from "next";
import type { NextPage } from "next";
import Head from "next/head";
import axios from "axios";
import Link from "next/link";

/*
export const getServerSideProps: GetServerSideProps = async () => {
  const { products, error } = await axios
    .get("http://localhost:8000/api/v1/shop/products/1")
    .then((response) => {
      console.log(response["data"]);
      return response;
    })
    .catch((error) => {
      console.log(error);
      return error;
    });

  return {
    props: {
      products,
    },
  };
};
*/

const Test: NextPage = () => {
  const product = {
    thumbnail: "http://localhost:8000/uploads/photos/green-tea.jpg",
    name: "Green Tea",
    description: "Best tea in the world...",
    price: 5.99,
    quantity: 10,
  };

  let products = [];

  for (let i = 0; i < 12; i++) {
    products.push(product);
  }

  return (
    <>
      <Head>
        <title>Tea Shop</title>
        <meta name="description" content="Made with NextJS" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Nav />

      <main className="flex flex-col items-center">
        <h1 className="mt-8 mb-16 text-7xl">Products</h1>
        <div className="flex flex-wrap justify-center items-center m-4 mb-16">
          {products.map((element: any, id: any) => (
            <div key={id} className="w-96 rounded-xl shadow-xl m-4 bg-gray-800">
              <img src={element.thumbnail} className="rounded-xl" />
              <div className="flex flex-col items-center w-full p-4">
                <p className="text-3xl font-bold">{element.name}</p>
                <div className="rounded-xl w-full p-4 mt-2 mb-4 bg-gray-700">
                  <p>{element.description}</p>
                </div>
                <div className="flex justify-between items-baseline w-full">
                  <p className="text-2xl font-semibold text-blue-200">$ {element.price}</p>
                  {element.quantity > 0 ? <p>{element.quantity} on stock</p> : <p className="text-red-500">Not on stock</p>}
                </div>
                <div className="flex justify-between items-baseline w-full mt-8">
                  <Link href={"product/" + id}>
                    <a className="p-4 mt-4 rounded-xl shadow-xl transition-all ease-linear duration-100 bg-gray-600 hover:bg-blue-200 hover:text-black">Learn more</a>
                  </Link>
                  <button type="button" className="p-4 mt-4 rounded-xl shadow-xl transition-all ease-linear duration-100 bg-blue-500 hover:bg-blue-200 hover:text-black">
                    Add to cart
                  </button>
                </div>
              </div>
            </div>
          ))}
        </div>
      </main>

      <Footer />
    </>
  );
};

export default Test;
