import { GetServerSideProps } from "next";
import axios from "axios";

const Index = (products: any) => {
  return (
    <>
      <p>My API</p>
      {/*
      products.map((element: any, id: any) => (
        <div key={id}>
          <p>{element.name}</p>
          <p>{element.description}</p>
          <p>{element.price}</p>
          <p>{element.quantity}</p>
        </div>
      ))
      //
        products.forEach((element: any) => {
          <div>
            <p>{element.name}</p>
            <p>{element.description}</p>
            <p>{element.price}</p>
            <p>{element.quantity}</p>
          </div>;
        })
       */}
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async () => {
  let products: any = null;
  let server_error: any = null;

  /*
  await axios
    .post("http://localhost:8000/api/v1/shop/user/register/", {
      email: "kosta-email@protonmail.com",
      password: "Secret-passw0rd",
      full_name: "Kosta",
      address: "Test City 101010",
      phone: "123456789",
    })
    .then((response) => {
      console.log(response.data);
      user = response.data;
    })
    .catch((error) => {
      server_error = error["error"];
    });
  */

  await axios
    .get("http://localhost:8000/api/v1/shop/products/1")
    .then((response) => {
      products = response.data;
      console.log(response["data"]);
    })
    .catch((error) => {
      server_error = error["error"];
    });

  console.log(products);

  /*
  const products = Object.keys(data).map((key) => {
    return { [key]: data[key] };
  });
  */

  const data: any = {
    name: "Green Tea",
    description: "Best tea in the world...",
    price: 5.99,
    quantity: 10,
  };

  return {
    props: {
      products,
    },
  };
};

export default Index;
