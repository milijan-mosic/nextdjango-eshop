import Link from "next/link";

const Nav = () => {
  return (
    <nav className="flex justify-between items-center w-full p-4 bg-gray-800">
      <Link href="/">
        <img src="https://cdn-icons.flaticon.com/png/512/869/premium/869432.png?token=exp=1656944308~hmac=a96a1a54d289267d1871bb66bde277a9" className="w-12 h-12 cursor-pointer" />
      </Link>

      <div className="flex justify-end items-baseline w-full">
        <Link href="/blog/">
          <a className="p-4 mr-2  hover:underline hover:underline-offset-1">Blog</a>
        </Link>
        <Link href="/">
          <a className="p-4 ml-2 mr-2  hover:underline hover:underline-offset-1">Products</a>
        </Link>
        <Link href="/user/register">
          <a className="p-4 ml-2 mr-2  rounded-xl shadow-xl transition-all ease-linear duration-100 bg-blue-500 hover:bg-blue-200 hover:text-black">Sign up</a>
        </Link>
        <Link href="/user/login">
          <a className="p-4 ml-2 mr-2 rounded-xl shadow-xl transition-all ease-linear duration-100 bg-white text-black hover:bg-blue-200 hover:text-black">Sign in</a>
        </Link>
        <div className="p-4 ml-2 rounded-xl shadow-xl transition-all ease-linear duration-100  bg-gray-600 hover:border-blue-200 hover:cursor-pointer hover:bg-blue-200 hover:text-black">
          <p>Your Cart</p>
        </div>
      </div>
    </nav>
  );
};

export default Nav;
