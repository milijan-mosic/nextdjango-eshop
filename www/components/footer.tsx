const Footer = () => {
  const Subscribe = async (event: any) => {
    event.preventDefault();
  };

  return (
    <footer className="flex justify-between items-baseline p-4 bg-gray-800">
      <div className="flex items-baseline">
        <h6 className="mr-4 font-semibold text-blue-200">Subscribe to newsletter {"->"}</h6>

        <form onSubmit={Subscribe}>
          <label>
            Name
            <input type="text" className="p-2 ml-2 mr-6 rounded-xl shadow-md bg-gray-700" />
          </label>

          <label>
            Email
            <input type="email" className="p-2 ml-2 mr-6 rounded-xl shadow-md bg-gray-700" />
          </label>

          <input type="submit" value="Join the club" className="pt-2 pb-2 pr-4 pl-4 rounded-xl shadow-xl transition-all ease-linear duration-100 cursor-pointer bg-blue-500 hover:bg-blue-200 hover:text-black" />
        </form>
      </div>

      <p>2022.</p>
    </footer>
  );
};

export default Footer;
